import http from "../conf-http";

class AuthService {
  getAll() {
    return http.get("/");
  }
}

export default new AuthService();