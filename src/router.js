import {
  createWebHistory,
  createRouter
} from "vue-router";

const routes = [{
    path: "/FindOffice",
    alias: "/FindOffice",
    name: "FindOffice",
    component: () => import("./components/client/SearchPage"),
  },
  {
    path: "/",
    alias: "/client",
    name: "Client",
    component: () => import("./components/client/MainPage"),
  },
  {
    path: "/admin/form",
    alias: "/admin",
    name: "Dashboard",
    component: () => import("./components/ExampleForm"),
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;